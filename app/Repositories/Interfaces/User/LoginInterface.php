<?php


namespace App\Repositories\Interfaces\User;





interface LoginInterface
{

    /**
     * @param  array  $input
     */
    public function loginAttempt(array $input);
}
