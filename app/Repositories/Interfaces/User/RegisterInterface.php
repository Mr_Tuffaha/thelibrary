<?php


namespace App\Repositories\Interfaces\User;





interface RegisterInterface
{

    /**
     * @param  array  $input
     */
    public function registerByEmail(array $input);
}
