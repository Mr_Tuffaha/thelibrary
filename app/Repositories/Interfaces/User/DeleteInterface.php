<?php


namespace App\Repositories\Interfaces\User;





interface DeleteInterface
{
    /**
     * @param  array  $input
     */
    public function getDestroyToken(array $input);

    /**
     * @param  array  $input
     */
    public function destroy(array $input);
}
