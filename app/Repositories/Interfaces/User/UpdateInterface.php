<?php


namespace App\Repositories\Interfaces\User;
use Illuminate\Http\Response;

interface UpdateInterface
{

    /**
     * @param  array  $input
     * @return Response
     */
    public function updateProfile(array $input);
}
