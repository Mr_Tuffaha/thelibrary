<?php


namespace App\Repositories\Interfaces;


interface CategoryInterface
{
    public function index();

    /**
     * @param $category_id
     */
    public function getPosts($category_id);
}
