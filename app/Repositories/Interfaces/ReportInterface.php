<?php


namespace App\Repositories\Interfaces;
use Illuminate\Http\Response;


interface ReportInterface
{
    /**
     * @param  array  $input
     * @return Response
     */
    public function reportUser(array $input);

    /**
     * @param  array  $input
     * @return Response
     */
    public function reportPost(array $input);

}
