<?php


namespace App\Repositories\Interfaces;
use Illuminate\Http\Response;


interface PostInterface
{
    /**
     * @param  array  $input
     * @return Response
     */
    public function store(array $input);

    /**
     * @param  $post_id
     * @return Response
     */
    public function destroy($post_id);

    /**
     * @return Response
     */
    public function index();
}
