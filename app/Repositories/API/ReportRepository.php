<?php

namespace App\Repositories\API;

use App\Post;
use App\Repositories\API\Interfaces\ReportInterface;
use App\User;
use Illuminate\Http\JsonResponse;

class ReportRepository implements ReportInterface
{


    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function reportUser(array $input): JsonResponse
    {
        $user = request()->user();
        if ($user) {
            if ($user->id == $input['user_id']) {
                return response()->json(['message' => 'You cant report your self!'], 403);
            }

            $userToBeReported = User::find($input['user_id']);

            if ($userToBeReported) {
                if (count($userToBeReported->reports()->where('user_id', $user->id)->get())) {
                    return response()->json(['message' => 'You have already reported this user!'], 403);
                }
                $report = $userToBeReported->reports()->create([
                    'report_type_id' => $input['report_type_id'], 'user_id' => $user->id
                ]);
                if ($report) {
                    return response()->json(['data' => ['report' => $report]], 201);
                } else {
                    return response()->json(['message' => 'Something went wrong'], 500);
                }
            } else {
                return response()->json(['message' => 'The user you want to report doesnt exist!'], 404);
            }
        } else {
            return response()->json(['message' => 'You have to be logged in!'], 401);
        }

    }

    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function reportPost(array $input): JsonResponse
    {
        $user = request()->user();
        if ($user) {
            $postToBeReported = Post::find($input['post_id']);
            if ($postToBeReported) {

                if ($user->id == $postToBeReported->user_id) {
                    return response()->json(['message' => 'You cant report your own posts!'], 403);
                }

                if (count($postToBeReported->reports()->where('user_id', $user->id)->get())) {
                    return response()->json(['message' => 'You have already reported this post!'], 403);
                }
                $report = $postToBeReported->reports()->create([
                    'report_type_id' => $input['report_type_id'],
                    'user_id' => $user->id
                ]);

                if ($report) {
                    return response()->json(['data' => ['report' => $report]], 201);
                } else {
                    return response()->json(['message' => 'Something went wrong'], 500);
                }
            } else {
                return response()->json(['message' => 'The post you want to report doesnt exist!'], 404);
            }
        } else {
            return response()->json(['message' => 'You have to be logged in!'], 401);
        }
    }
}
