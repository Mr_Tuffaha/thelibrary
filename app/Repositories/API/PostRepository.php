<?php


namespace App\Repositories\API;


use App\Post;
use App\Repositories\API\Interfaces\PostInterface;
use Illuminate\Http\JsonResponse;

class PostRepository implements PostInterface
{

    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function store(array $input): JsonResponse
    {
        $user = request()->user();

        if (!$user){
            return response()->json(['message' => 'there was an internal problem'],500);
        }

        $post = $user->posts()->create($input);

        if (!$post){
            return response()->json(['message' => 'there was an internal problem'],500);
        }

        return response()->json(['data' => ['post' => $post]],200);
    }

    /**
     * @param  $post_id
     * @return JsonResponse
     */
    public function destroy($post_id): JsonResponse
    {
        $post = Post::find($post_id);

        if (!$post){
            return response()->json(['message' => 'post not found'],404);
        }

        $user = request()->user();

        if (!$user){
            return response()->json(['message' => 'there was an internal problem'],500);
        }

        if ($post->user_id == $user->id){
            $post->delete();
        }else{
            return response()->json(['message'=>'You dont own this post'],403);
        }

        return response()->json(['message'=>'post deleted successfully'],200);
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $posts = Post::all()->sortByDesc('created_at')->values()->all();
        return response()->json(['posts'=>$posts],200);
    }
}
