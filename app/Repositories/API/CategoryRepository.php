<?php


namespace App\Repositories\API;


use App\Category;
use App\Post;
use App\Repositories\API\Interfaces\CategoryInterface;
use Illuminate\Http\JsonResponse;

class CategoryRepository implements CategoryInterface
{


    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $categories = Category::has('children')
            ->select('id', 'name')
            ->with('children:id,name,parent_id')
            ->orderBy('parent_id')
            ->get();
        return response()->json(['data' => ['categories' => $categories]], 200);
    }

    /**
     * @param $category_id
     * @return JsonResponse
     */
    public function getPosts($category_id): JsonResponse
    {
        $categoryFound = Category::find($category_id);

        if (!$categoryFound) {
            return response()->json(['message' => 'invalid category id'], 404);
        }

        if ($categoryFound->parent_id) {
            $posts = $this->getChildCategoryPosts($categoryFound);

        } else {
            $posts = $this->getRootCategoryPosts($categoryFound);
        }

        return response()->json([
            'data' => $this->preparePostsList($categoryFound,$posts)
        ], 200);
    }

    protected function preparePostsList($category,$posts)
    {
        return [
            'category' => [
                'id' => $category->id,
                'name' => $category->name,
                'posts_count' => count($posts),
                'posts' => $posts
            ]
        ];
    }

    protected function getChildCategoryPosts($category)
    {
        return $category->posts()
            ->with('category:id,name')
            ->get();
    }

    protected function getRootCategoryPosts($category)
    {
        $categoryIds = Category::where('categories.id', $category->id)
            ->join('categories as temp', 'categories.id', '=', 'temp.parent_id')
            ->select('temp.id')
            ->get()
            ->toArray();

        return Post::whereIn('category_id', $categoryIds)
            ->with('category:id,name')
            ->get();
    }
}
