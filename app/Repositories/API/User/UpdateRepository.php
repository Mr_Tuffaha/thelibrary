<?php


namespace App\Repositories\API\User;


use App\Repositories\API\Interfaces\User\UpdateInterface;
use Illuminate\Http\JsonResponse;

class UpdateRepository implements UpdateInterface
{


    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function updateProfile(array $input): JsonResponse
    {
        $user = request()->user();

        if (!$user){
            return response()->json(['message' => 'there was an internal problem'],500);
        }

        if ($user->update($input)){
            return response()->json(['message' => 'User updated successfully '],200);
        }

        return response()->json(['message' => 'there was an internal problem'],500);
    }
}
