<?php


namespace App\Repositories\API\User;


use App\Repositories\API\Interfaces\User\RegisterInterface;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

class RegisterRepository implements RegisterInterface
{
    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function registerByEmail(array $input): JsonResponse
    {
        $input['password'] = Hash::make($input['password']);
        $user = User::create($input);
        $user->sendEmailVerificationNotification();
        return response()->json($user,200);
    }
}
