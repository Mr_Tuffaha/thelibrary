<?php


namespace App\Repositories\API\User;


use App\Repositories\API\Interfaces\User\LoginInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class LoginRepository implements LoginInterface
{

    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function loginAttempt(array $input): JsonResponse
    {
        if (Auth::attempt($input)) {
            $user = Auth::user();
            if ($user) {
                $token = 'Bearer '.$user->createToken('login')->accessToken;

                return response()->json(['data' => ['token' => $token]], 200);
            }
        }
        return response()->json(["error" => ["message" => "Email or password is wrong"]], 401);
    }
}
