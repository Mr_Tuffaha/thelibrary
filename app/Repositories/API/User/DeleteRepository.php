<?php


namespace App\Repositories\API\User;


use App\Repositories\API\Interfaces\User\DeleteInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DeleteRepository implements DeleteInterface
{


    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function destroy(array $input): JsonResponse
    {
        $user = request()->user();
        if ($user) {
            $shortToken = $user->shortTokens()
                ->where('type','delete-user')
                ->where('token', $input['token'])
                ->whereTime('expires_at','>',now())
                ->first();

            if ($shortToken) {
                $user->delete();
                return response()->json(['message' => 'User deleted successfully'], 200);
            } else {
                return response()->json(['message' => 'invalid token'], 401);
            }
        } else {
            return response()->json(['message' => 'Not authenticated'], 401);
        }
    }


    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function getDestroyToken(array $input): JsonResponse
    {
        $user = request()->user();
        if ($user) {
            if (Hash::check($input['password'], $user->password)) {
                $shortToken = $user->shortTokens()->create([
                    'token' => Str::random(),
                    'type' => 'delete-user',
                    'expires_at' => now()->addMinute(2),
                ]);
                return response()->json(['data' => ['token' => $shortToken->token]], 200);
            } else {
                return response()->json(["error" => ["message" => "password is wrong"]], 401);
            }
        } else {
            return response()->json(['message' => 'Not authenticated'], 401);
        }
    }
}
