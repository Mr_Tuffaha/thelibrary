<?php


namespace App\Repositories\API\Interfaces;


use App\Repositories\Interfaces\ReportInterface as MainReportInterface;
use Illuminate\Http\JsonResponse;

interface ReportInterface extends MainReportInterface
{
    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function reportUser(array $input): JsonResponse;


    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function reportPost(array $input): JsonResponse;

}
