<?php


namespace App\Repositories\API\Interfaces\User;

use App\Repositories\Interfaces\User\LoginInterface as MainUserLoginInterface;
use Illuminate\Http\JsonResponse;


interface LoginInterface extends MainUserLoginInterface
{
    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function loginAttempt(array $input): JsonResponse;
}
