<?php


namespace App\Repositories\API\Interfaces\User;

use App\Repositories\Interfaces\User\RegisterInterface as MainUserRegisterInterface;
use Illuminate\Http\JsonResponse;


interface RegisterInterface extends MainUserRegisterInterface
{
    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function registerByEmail(array $input): JsonResponse;
}
