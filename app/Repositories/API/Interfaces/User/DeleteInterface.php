<?php


namespace App\Repositories\API\Interfaces\User;

use App\Repositories\Interfaces\User\DeleteInterface as MainUserDeleteInterface;
use Illuminate\Http\JsonResponse;


interface DeleteInterface extends MainUserDeleteInterface
{

    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function getDestroyToken(array $input): JsonResponse;

    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function destroy(array $input): JsonResponse;
}
