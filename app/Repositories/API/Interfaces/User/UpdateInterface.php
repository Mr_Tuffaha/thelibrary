<?php


namespace App\Repositories\API\Interfaces\User;

use App\Repositories\Interfaces\User\UpdateInterface as MainUpdateInterface;
use Illuminate\Http\JsonResponse;


interface UpdateInterface extends MainUpdateInterface
{

    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function updateProfile(array $input): JsonResponse;
}
