<?php


namespace App\Repositories\API\Interfaces;


use App\Repositories\Interfaces\CategoryInterface as MainCategoryInterface;
use Illuminate\Http\JsonResponse;

interface CategoryInterface extends MainCategoryInterface
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse;

    /**
     * @param $category_id
     * @return JsonResponse
     */
    public function getPosts($category_id): JsonResponse;
}
