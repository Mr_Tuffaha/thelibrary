<?php


namespace App\Repositories\API\Interfaces;


use Illuminate\Http\JsonResponse;

interface PostInterface
{
    /**
     * @param  array  $input
     * @return JsonResponse
     */
    public function store(array $input): JsonResponse;


    /**
     * @param  $post_id
     * @return JsonResponse
     */
    public function destroy($post_id): JsonResponse;

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse;
}
