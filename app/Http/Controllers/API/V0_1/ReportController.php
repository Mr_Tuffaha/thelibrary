<?php


namespace App\Http\Controllers\API\V0_1;


use App\Http\Controllers\Controller;
use App\Http\Requests\Report\ReportPostRequest;
use App\Http\Requests\Report\ReportUserRequest;
use App\Repositories\API\Interfaces\ReportInterface;
use Illuminate\Http\JsonResponse;

class ReportController extends Controller
{
    /**
     * @var ReportInterface
     */
    private $reportRepository;

    public function __construct(
        ReportInterface $reportRepository
    ) {
        $this->reportRepository = $reportRepository;
    }

    /**
     * @param  ReportUserRequest  $request
     * @return JsonResponse
     */
    public function reportUser(ReportUserRequest $request)
    {
        return $this->reportRepository->reportUser($request->validated());
    }

    /**
     * @param  ReportPostRequest  $request
     * @return JsonResponse
     */
    public function reportPost(ReportPostRequest $request)
    {
        return $this->reportRepository->reportPost($request->validated());
    }

}
