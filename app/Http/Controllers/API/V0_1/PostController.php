<?php

namespace App\Http\Controllers\API\V0_1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Post\StoreRequest;
use App\Repositories\API\Interfaces\PostInterface;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    /**
     * @var PostInterface
     */
    private $postRepository;

    /**
     * PostController constructor.
     * @param  PostInterface  $postRepository
     */
    public function __construct(
        PostInterface $postRepository
    ) {
        $this->postRepository = $postRepository;
    }

    /**
     * @param  StoreRequest  $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request)
    {
        return $this->postRepository->store($request->validated());
    }

    /**
     * @param $post_id
     * @return JsonResponse
     */
    public function destroy($post_id)
    {
        return $this->postRepository->destroy($post_id);
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return $this->postRepository->index();
    }
}
