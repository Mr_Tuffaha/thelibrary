<?php

namespace App\Http\Controllers\API\V0_1;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\DestroyRequest;
use App\Http\Requests\User\DestroyTokenRequest;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Requests\User\UpdateRequest;
use App\Repositories\API\Interfaces\User\UpdateInterface;
use App\Repositories\API\Interfaces\User\LoginInterface;
use App\Repositories\API\Interfaces\User\RegisterInterface;
use App\Repositories\API\Interfaces\User\DeleteInterface;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    /**
     * @var RegisterInterface
     */
    protected $userRegisterRepository;

    /**
     * @var LoginInterface
     */
    protected $userLoginRepository;

    /**
     * @var LoginInterface
     */
    protected $updateRepository;

    /**
     * @var DeleteInterface
     */
    protected $deleteRepository;

    /**
     * UserLoginController constructor.
     * @param  LoginInterface  $userLoginRepository
     * @param  RegisterInterface  $userRegisterRepository
     * @param  UpdateInterface  $updateRepository
     * @param  DeleteInterface  $deleteRepository
     */
    public function __construct(
        LoginInterface $userLoginRepository,
        RegisterInterface $userRegisterRepository,
        UpdateInterface $updateRepository,
        DeleteInterface $deleteRepository
    ) {
        $this->userLoginRepository = $userLoginRepository;
        $this->userRegisterRepository = $userRegisterRepository;
        $this->updateRepository = $updateRepository;
        $this->deleteRepository = $deleteRepository;
    }

    /**
     * @param  LoginRequest  $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request)
    {
        return $this->userLoginRepository->loginAttempt($request->validated());
    }

    /**
     * @param  RegisterRequest  $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        return $this->userRegisterRepository->registerByEmail($request->validated());
    }

    /**
     * @param  UpdateRequest  $request
     * @return JsonResponse
     */
    public function updateInfo(UpdateRequest $request)
    {
        return $this->updateRepository->updateProfile($request->validated());
    }

    /**
     * @param  DestroyRequest  $request
     * @return JsonResponse
     */
    public function destroy(DestroyRequest $request)
    {
        return $this->deleteRepository->destroy($request->validated());
    }

    /**
     * @param  DestroyTokenRequest  $request
     * @return JsonResponse
     */
    public function getDestroyToken(DestroyTokenRequest $request)
    {
        return $this->deleteRepository->getDestroyToken($request->validated());
    }
}
