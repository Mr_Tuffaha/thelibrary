<?php

namespace App\Http\Controllers\API\V0_1;

use App\Http\Controllers\Controller;
use App\Repositories\API\Interfaces\CategoryInterface;
use Illuminate\Http\JsonResponse;

class CategoryController extends Controller
{
    /**
     * @var CategoryInterface
     */
    private $categoryRepository;

    /**
     * PostController constructor.
     * @param  CategoryInterface  $categoryRepository
     */
    public function __construct(
        CategoryInterface $categoryRepository
    ) {
        $this->categoryRepository = $categoryRepository;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return $this->categoryRepository->index();
    }

    /**
     * @param $category_id
     * @return JsonResponse
     */
    public function getPosts($category_id)
    {
        return $this->categoryRepository->getPosts($category_id);
    }
}
