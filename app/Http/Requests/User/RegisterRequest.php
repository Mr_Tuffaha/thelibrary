<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'      => 'required|email|unique:users',
            'username'   => 'required|min:5|max:30|regex:/[A-Za-z0-9_]+/|unique:users',
            'birth_date' => 'required|date|regex:/[0-9]{4}-[0-9]{2}-[0-9]{2}/|before_or_equal:-13 years',
            'password'   => 'required|min:8',
            'repassword' => 'required|same:password',
            'name'       => 'required'
        ];
    }
}
