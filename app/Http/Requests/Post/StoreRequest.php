<?php


namespace App\Http\Requests\Post;

use App\Category;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'body' => 'required',
            'category_id' => [
                'bail',
                'required',
                'integer',
                'exists:App\Category,id',
                function ($attributeName, $attributeValue, $failCallback) {
                    $category = Category::find($attributeValue);
                    if (!$category) {
                        $failCallback("The selected $attributeName is invalid");
                    } elseif ($category->parent_id == 0) {
                        $failCallback("The selected $attributeName is invalid as its a root category");
                    }
                }
            ],
        ];
    }
}
