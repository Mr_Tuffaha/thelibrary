<?php

namespace App\Providers;


use App\Repositories\API\Interfaces\User\LoginInterface as APIUserLoginInterface;
use App\Repositories\API\User\LoginRepository as APIUserLoginRepository;

use App\Repositories\API\Interfaces\User\RegisterInterface as APIUserRegisterInterface;
use App\Repositories\API\User\RegisterRepository as APIUserRegisterRepository;

use App\Repositories\API\Interfaces\User\UpdateInterface as APIUserUpdateInterface;
use App\Repositories\API\User\UpdateRepository as APIUserUpdateRepository;

use App\Repositories\API\Interfaces\User\DeleteInterface as APIUserDeleteInterface;
use App\Repositories\API\User\DeleteRepository as APIUserDeleteRepository;

use App\Repositories\API\Interfaces\PostInterface as APIPostInterface;
use App\Repositories\API\PostRepository as APIPostRepository;

use App\Repositories\API\Interfaces\ReportInterface as APIReportInterface;
use App\Repositories\API\ReportRepository as APIReportRepository;

use App\Repositories\API\Interfaces\CategoryInterface as APICategoryInterface;
use App\Repositories\API\CategoryRepository as APICategoryRepository;

use Illuminate\Support\ServiceProvider;

class APIRepositoryBindingsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        $this->app->bind(APIUserLoginInterface::class, APIUserLoginRepository::class);
        $this->app->bind(APIUserRegisterInterface::class, APIUserRegisterRepository::class);
        $this->app->bind(APIUserUpdateInterface::class, APIUserUpdateRepository::class);
        $this->app->bind(APIUserDeleteInterface::class, APIUserDeleteRepository::class);
        $this->app->bind(APIPostInterface::class, APIPostRepository::class);
        $this->app->bind(APIReportInterface::class, APIReportRepository::class);
        $this->app->bind(APICategoryInterface::class, APICategoryRepository::class);
    }
}
