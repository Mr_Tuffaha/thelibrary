<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    public function reportType()
    {
        return $this->belongsTo(ReportType::class);
    }

    public function reportable()
    {
        return $this->morphTo();
    }

    protected $fillable = [
        'user_id',
        'report_type_id',
    ];
}
