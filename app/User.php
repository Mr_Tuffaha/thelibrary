<?php

namespace App;

use App\Traits\Reportable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable, Reportable;

    public function posts(){
        return $this->hasMany(Post::class);
    }

    public function shortTokens(){
        return $this->hasMany(ShortToken::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'username',
        'password',
        'birth_date',
        'phone_number',
        'website',
        'bio',

    ];

    /**
     * The attributes that are dates so that
     * they are converted to carbon.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'birth_date'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
