<?php


namespace App\Traits;


use App\Report;

trait Reportable
{
    public function reports()
    {
        return $this->morphMany(Report::class, 'reportable');
    }
}