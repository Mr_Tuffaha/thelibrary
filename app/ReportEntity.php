<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportEntity extends Model
{
    //
    public function reportTypes()
    {
        return $this->hasMany(ReportType::class);
    }
}
