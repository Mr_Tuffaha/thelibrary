<?php

namespace App;

use App\Traits\Reportable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Reportable;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'body',
        'category_id',
    ];
}
