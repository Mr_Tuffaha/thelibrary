<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShortToken extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'token',
        'expires_at',
    ];
}
