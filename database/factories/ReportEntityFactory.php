<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Post;
use App\ReportEntity;
use App\User;

$factory->defineAs(ReportEntity::class, 'user', function () {
    return [
        'entity'=> User::class,
    ];
});

$factory->defineAs(ReportEntity::class, 'post', function () {
    return [
        'entity'=> Post::class,
    ];
});
