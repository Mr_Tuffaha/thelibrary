<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ReportEntity;
use Faker\Generator as Faker;
use App\ReportType;

$factory->define(ReportType::class, function (Faker $faker) {
    return [
        'report_entity_id' => ReportEntity::all()->random()->id,
        'title' => $faker->sentence,
    ];
});