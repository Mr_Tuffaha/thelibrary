<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('report_type_id')->index();
            $table->bigInteger('user_id')->index();
            $table->bigInteger('reportable_id');
            $table->string('reportable_type');
            $table->text('body')->nullable();
            $table->boolean('is_checked')->default(0);
            $table->boolean('is_valid')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->index(['reportable_type','reportable_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
