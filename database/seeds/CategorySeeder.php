<?php

use App\Category;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws FileNotFoundException
     */
    public function run()
    {
        $categoriesJson = Storage::disk('local')->get('categories_list.json');
        $categoriesArray = json_decode($categoriesJson);
        foreach ($categoriesArray as $categoryName => $children) {
            $foundCategories = Category::doesntHave('parent')->where('name', $categoryName);
            if (!$foundCategories->count()) {
                $category = factory(Category::class)->create([
                    'name' => $categoryName,
                ]);
            } else {
                $category = $foundCategories->get()->first();
            }
            foreach ($children as $childCategoryName) {
                $foundCategories = Category::has('parent')->where('name', $childCategoryName)->where('parent_id',
                    $category->id);
                if (!$foundCategories->count()) {
                    factory(Category::class)->create([
                        'name' => $childCategoryName,
                        'parent_id' => $category->id,
                    ]);
                }
            }
        }
    }
}
