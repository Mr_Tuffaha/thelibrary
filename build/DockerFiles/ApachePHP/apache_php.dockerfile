FROM php:7.2-apache

RUN a2enmod rewrite

# Install dependencies
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    locales \
    zip \
    jpegoptim optipng pngquant gifsicle \
    vim \
    nano \
    unzip \
    git \
    curl

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install extensions
RUN docker-php-ext-install gd pdo_mysql mbstring zip exif pcntl
RUN docker-php-ext-configure gd \
    --with-gd \
    --with-freetype-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ \
    --with-png-dir=/usr/include/

RUN pecl install redis && docker-php-ext-enable redis

RUN docker-php-ext-install opcache
COPY ./build/DockerFiles/ApachePHP/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

ENV PHP_OPCACHE_VALIDATE_TIMESTAMPS="0" \
    PHP_OPCACHE_MAX_ACCELERATED_FILES="10000" \
    PHP_OPCACHE_MEMORY_CONSUMPTION="192" \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE="10"

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
COPY ./build/DockerFiles/ApachePHP/website.conf /etc/apache2/sites-available/

RUN a2dissite *

COPY --chown=www-data:www-data ./build/DockerFiles/ApachePHP/start.sh /usr/local/bin/start
RUN chmod +x /usr/local/bin/start

RUN rm -rf /var/www/html
COPY ./ /var/www/
RUN mkdir --parents /var/www/storage/framework/cache/data
WORKDIR /var/www/
RUN mkdir --parents storage/framework/cache/data
RUN find . -type f -exec chown www-data:www-data {} \; -exec chmod 644 {} \;
RUN find . -type d -exec chown www-data:www-data {} \; -exec chmod 755 {} \;
USER www-data
RUN composer install
RUN php artisan optimize:clear
USER root
RUN chmod -R g+w storage/ bootstrap/cache/

ENTRYPOINT ["/usr/local/bin/start"]

