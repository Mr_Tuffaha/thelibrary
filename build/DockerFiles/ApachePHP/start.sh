#!/bin/bash
docker-php-entrypoint

sed "s/{{SERVER_NAME}}/${SERVER_NAME:-localhost}/" /etc/apache2/sites-available/website.conf > /tmp/tempfile && mv /tmp/tempfile /etc/apache2/sites-available/website.conf
sed "s/{{SERVER_EMAIL}}/${SERVER_EMAIL:-example@example.com}/" /etc/apache2/sites-available/website.conf > /tmp/tempfile && mv /tmp/tempfile /etc/apache2/sites-available/website.conf
a2ensite website.conf

echo -e ${PASSPORT_PRIVATE_KEY} > /var/www/storage/oauth-private.key
echo -e ${PASSPORT_PUBLIC_KEY} > /var/www/storage/oauth-public.key

php artisan passport:install

if [ ${APP_ENV:-production} == "production" ]; then
    echo "caching configeration" \
    && php artisan optimize \
    && php artisan view:cache

    find storage/ bootstrap/cache/ -type f -exec chown www-data:www-data {} \; -exec chmod 664 {} \;
    find storage/ bootstrap/cache/ -type d -exec chown www-data:www-data {} \; -exec chmod 775 {} \;
fi

apache2-foreground
