#!/bin/sh

IFS=$'\n'       # make newlines the only separator
set -f          # disable globbing
for i in $(grep -vE '^($|#)' .env); do
  export "$i"
done
unset IFS
