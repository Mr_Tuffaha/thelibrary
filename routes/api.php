<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API\\V0_1')->prefix('0.1')->group(function () {
    Route::get('posts', 'PostController@index');

    Route::get('category', 'CategoryController@index');
    Route::get('category/{category_id}/posts', 'CategoryController@getPosts');

    Route::post('user/login', 'UserController@login');
    Route::post('user/register', 'UserController@register');

    Route::middleware('auth:api')->group(function () {
        Route::post('post/store', 'PostController@store');
        Route::delete('post/{post_id}', 'PostController@destroy');
        Route::patch('user/info', 'UserController@updateInfo');

        Route::post('report/user', 'ReportController@reportUser');
        Route::post('report/post', 'ReportController@reportPost');

        Route::post('user/delete','UserController@getDestroyToken');
        Route::delete('user/delete','UserController@destroy');
    });
});


