<?php


namespace Tests\Feature\API;


use App\ReportEntity;
use App\ReportType;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;

class ReportUserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected $baseURL = '/api/0.1/';

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
        $this->makeReportEntitiesAndTypes();
        $this->makeUsers(10);
    }

    public function testReportSuccessfulUserReporting()
    {

        $randomUserId = User::all()->random()->id;

        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'report/user',
            [
                'report_type_id' => $this->getRandomReportId(User::class),
                'user_id' => $randomUserId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(201);
    }

    public function testReportingUnknownUser()
    {

        $user = factory(User::class)->create();
        $randomUserId = $user->id + 10;
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'report/user',
            [
                'report_type_id' => $this->getRandomReportId(User::class),
                'user_id' => $randomUserId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(404);
    }

    public function testReportingSelf()
    {
        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'report/user',
            [
                'report_type_id' => $this->getRandomReportId(User::class),
                'user_id' => $user->id,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(403);
    }

    public function testReportingUserMultipleTimes()
    {
        $randomUserId = User::all()->random()->id;

        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'report/user',
            [
                'report_type_id' => $this->getRandomReportId(User::class),
                'user_id' => $randomUserId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(201);

        $response = $this->post(
            $this->baseURL.'report/user',
            [
                'report_type_id' => $this->getRandomReportId(User::class),
                'user_id' => $randomUserId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(403);

        $response = $this->post(
            $this->baseURL.'report/user',
            [
                'report_type_id' => $this->getRandomReportId(User::class),
                'user_id' => $randomUserId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(403);
    }

    protected function getPostRequestHeaders(string $accessToken = ''): array
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => 'Bearer '.$accessToken,
        ];
    }

    protected function makeUsers(int $number = 1)
    {
        factory(User::class, $number)->create();
    }

    protected function makeReportEntitiesAndTypes()
    {
        factory(ReportEntity::class, 'user')->create();
        factory(ReportType::class, 10)->create();
    }

    protected function getRandomReportId($type)
    {
        return ReportEntity::where('entity', $type)->first()->reportTypes->random()->id;
    }
}
