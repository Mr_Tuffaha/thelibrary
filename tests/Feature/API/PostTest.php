<?php


namespace Tests\Feature\API;


use App\Category;
use App\Post;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected $baseURL = '/api/0.1/';

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
        $this->artisan('db:seed --class=CategorySeeder');
    }

    public function testPostSuccessfulStoring()
    {
        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;
        $response = $this->post(
            $this->baseURL.'post/store',
            $this->getFakerPostData(),
            $this->getPostRequestHeaders($accessToken)
        );
        $response->assertStatus(200);
    }

    public function testPostStoringWithUnauthorizedUser()
    {
        $response = $this->post(
            $this->baseURL.'post/store',
            $this->getFakerPostData(),
            $this->getPostRequestHeaders()
        );
        $response->assertStatus(401);
    }

    public function testPostSuccessfulDeletion()
    {
        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;
        factory(Post::class)->create([
            'user_id' => $user->id
        ]);
        $post = $user->posts->random();
        $response = $this->delete(
            $this->baseURL.'post/'.$post->id,
            [],
            $this->getPostRequestHeaders($accessToken)
        );
        $response->assertStatus(200);
    }

    public function testPostFailedDeletion()
    {

        $response = $this->delete(
            $this->baseURL.'post/'.rand(0, 1000),
            $this->getFakerPostData(),
            $this->getPostRequestHeaders()
        );
        $response->assertStatus(401);

        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;


        $response = $this->delete(
            $this->baseURL.'post/'.rand(0, 1000),
            $this->getFakerPostData(),
            $this->getPostRequestHeaders($accessToken)
        );
        $response->assertStatus(404);


        factory(Post::class)->create([
            'user_id' => $user->id + 2
        ]);

        $post = Post::first();
        $response = $this->delete(
            $this->baseURL.'post/'.$post->id,
            $this->getFakerPostData(),
            $this->getPostRequestHeaders($accessToken)
        );
        $response->assertStatus(403);

    }

    public function testListPosts()
    {
        factory(User::class, 10)->create();
        factory(Post::class, 100)->create();
        $response = $this->get(
            $this->baseURL.'posts',
            $this->getPostRequestHeaders()
        );
        $response->assertStatus(200);
    }

    protected function getPostRequestHeaders(string $accessToken = ''): array
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => 'Bearer '.$accessToken,
        ];
    }

    protected function getFakerPostData(): array
    {
        return [
            'title' => $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'category_id' => Category::has('parent')->get()->random()->id,
        ];
    }
}
