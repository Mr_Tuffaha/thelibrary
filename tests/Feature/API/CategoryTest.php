<?php


namespace Tests\Feature\API;


use App\Category;
use App\Post;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTest extends TestCase
{
    use RefreshDatabase;

    protected $baseURL = '/api/0.1/';

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
        $this->artisan('db:seed --class=CategorySeeder');
        $this->makeUsersAndPosts(25);
    }

    public function testGetCategoriesList()
    {
        $response = $this->get(
            $this->baseURL.'category',
            $this->getGetRequestHeaders()
        );

        $response->assertStatus(200);
    }

    public function testGetPostsInCategory()
    {

        for ($i = 0; $i < 10; $i++) {
            $randomCategoryId = $this->getRandomCategoryId();

            $response = $this->get(
                $this->baseURL.'category/'.$randomCategoryId.'/posts',
                $this->getGetRequestHeaders()
            );

            $response->assertStatus(200);
        }

    }

    public function testGetPostsInInvalidCategory()
    {

        for ($i = 0; $i < 10; $i++) {
            $randomCategoryId = $this->getRandomInvalidCategoryId();

            $response = $this->get(
                $this->baseURL.'category/'.$randomCategoryId.'/posts',
                $this->getGetRequestHeaders()
            );

            $response->assertStatus(404);
        }

    }

    protected function getGetRequestHeaders(string $accessToken = ''): array
    {
        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$accessToken,
        ];
    }

    protected function makeUsersAndPosts(int $number = 1)
    {
        factory(User::class, $number)->create();
        factory(Post::class, $number * $number)->create();
    }

    protected function getRandomCategoryId()
    {
        return Category::all()->random()->id;
    }

    protected function getRandomInvalidCategoryId()
    {
        return Category::orderByDesc('id')->limit(1)->get()->first()->id + rand(1, 1000);
    }
}
