<?php

namespace Tests\Feature\API;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\User;

class UserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    private $baseURL = '/api/0.1/';

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
    }

    public function testUserSuccessfulRegistration()
    {
        $response = $this->post(
            $this->baseURL.'user/register',
            $this->getFakeUserData(),
            $this->getPostHeaders()
        );

        $response->assertStatus(200);
        $this->assertCount(1, User::all());
    }

    public function testUserFailedRegistration()
    {
        $data = $this->getFakeUserData();

        foreach ($data as $key => $value){
            $tempData = $data;
            $tempData[$key] = '';
            $response = $this->post(
                $this->baseURL.'user/register',
                $tempData,
                $this->getPostHeaders()
            );
            $response->assertStatus(422);
            $this->assertCount(0, User::all());
        }

    }

    public function testUserSuccessfulLogin()
    {
        $user = factory(User::class)->create();

        $this->assertCount(1, User::all());
        $response = $this->post(
            $this->baseURL.'user/login',
            [
                'email' => $user->email,
                'password' => 'password',
            ],
            $this->getPostHeaders()
        );
        $response->assertStatus(200);
    }

    public function testUserFailedLogin()
    {
        $user = factory(User::class)->create();
        $this->assertCount(1, User::all());

        $response = $this->post($this->baseURL.'user/login',
            [
                'email' => $user->email,
                'password' => 'password1',
            ],
            $this->getPostHeaders()
        );
        $response->assertStatus(401);

        $response = $this->post(
            $this->baseURL.'user/login',
            [
                'email' => '12'.$user->email,
                'password' => 'password',
            ],
            $this->getPostHeaders()
        );
        $response->assertStatus(401);

        $response = $this->post(
            $this->baseURL.'user/login',
            [
                'email' => '',
                'password' => 'password',
            ],
            $this->getPostHeaders()
        );
        $response->assertStatus(422);

        $response = $this->post(
            $this->baseURL.'user/login',
            [
                'email' => '12'.$user->email,
                'password' => '',
            ],
            $this->getPostHeaders()
        );
        $response->assertStatus(422);
    }

    public function testUpdateInfo()
    {
        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->patch(
            $this->baseURL.'user/info',
            $this->getFakerInfo(),
            $this->getPostHeaders($accessToken)
        );

        $response->assertStatus(200);
    }

    private function getFakeUserData()
    {
        return [
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'username' => $this->faker->unique()->userName,
            'birth_date' => $this->faker->dateTimeBetween('-30 years','-13 years')->format('Y-m-d'),
            'password' => 'password',
            'repassword' => 'password',
        ];
    }

    private function getFakerInfo()
    {
        return [
            'username' => $this->faker->userName,
            'bio' => substr($this->faker->paragraph,0,200),
        ];
    }

    protected function getPostHeaders(string $accessToken = ''): array
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => 'Bearer '.$accessToken,
        ];
    }
}
