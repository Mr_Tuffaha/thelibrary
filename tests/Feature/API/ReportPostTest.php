<?php


namespace Tests\Feature\API;


use App\Post;
use App\ReportEntity;
use App\ReportType;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReportPostTest extends TestCase
{
    use RefreshDatabase;

    protected $baseURL = '/api/0.1/';

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
        $this->artisan('db:seed --class=CategorySeeder');
        $this->makeReportEntitiesAndTypes();
        $this->makeUsersAndPosts(10);
    }

    public function testReportSuccessfulPostReporting()
    {

        $randomPostId = Post::all()->random()->id;

        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'report/post',
            [
                'report_type_id' => $this->getRandomReportId(Post::class),
                'post_id' => $randomPostId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(201);
    }

    public function testReportingUnknownPost()
    {

        $user = factory(User::class)->create();
        $randomPostId = Post::all()->last()->id + 10;
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'report/post',
            [
                'report_type_id' => $this->getRandomReportId(Post::class),
                'post_id' => $randomPostId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(404);
    }

    public function testReportingSelfPosts()
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->create([
            'user_id' => $user->id,
        ]);
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'report/post',
            [
                'report_type_id' => $this->getRandomReportId(Post::class),
                'post_id' => $post->id,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(403);
    }

    public function testReportingPostMultipleTimes()
    {
        $randomPostId = Post::all()->random()->id;

        $user = factory(User::class)->create();
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'report/post',
            [
                'report_type_id' => $this->getRandomReportId(Post::class),
                'post_id' => $randomPostId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(201);

        $response = $this->post(
            $this->baseURL.'report/post',
            [
                'report_type_id' => $this->getRandomReportId(Post::class),
                'post_id' => $randomPostId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(403);

        $response = $this->post(
            $this->baseURL.'report/post',
            [
                'report_type_id' => $this->getRandomReportId(Post::class),
                'post_id' => $randomPostId,
            ],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(403);
    }

    protected function getPostRequestHeaders(string $accessToken = ''): array
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => 'Bearer '.$accessToken,
        ];
    }

    protected function makeUsersAndPosts(int $number = 1)
    {
        factory(User::class, $number)->create();
        factory(Post::class, $number * $number)->create();
    }

    protected function makeReportEntitiesAndTypes()
    {
        factory(ReportEntity::class, 'post')->create();
        factory(ReportType::class, 10)->create();
    }

    protected function getRandomReportId($type)
    {
        return ReportEntity::where('entity', $type)->first()->reportTypes->random()->id;
    }
}
