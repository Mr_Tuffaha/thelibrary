<?php


namespace Tests\Feature\API;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    private $baseURL = '/api/0.1/';

    protected function setUp(): void
    {
        parent::setUp();
        $this->artisan('passport:install');
    }

    public function testUserDelete()
    {
        $user = factory(User::class)->create();
        $this->assertNotNull($user);

        $user_id = $user->id;
        $accessToken = $user->createToken('login')->accessToken;
        $response = $this->post(
            $this->baseURL.'user/delete',
            ['password' => 'password'],
            $this->getPostRequestHeaders($accessToken)
        );
        $response->assertStatus(200);

        $this->assertCount(1, $user->shortTokens);


        $token = $response->json('data')['token'];

        $response = $this->delete(
            $this->baseURL.'user/delete',
            ['token' => $token],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(200);

        $user = User::find($user_id);
        $this->assertNull($user);
    }

    public function testUserDeleteWrongToken()
    {
        $user = factory(User::class)->create();
        $this->assertNotNull($user);

        $user_id = $user->id;
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'user/delete',
            ['password' => 'password'],
            $this->getPostRequestHeaders($accessToken)
        );
        $response->assertStatus(200);

        $this->assertCount(1, $user->shortTokens);

        $token = $response->json('data')['token'];

        $response = $this->delete(
            $this->baseURL.'user/delete',
            ['token' => $token.'test'],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(401);

        $user = User::find($user_id);
        $this->assertNotNull($user);
    }

    public function testUserDeleteLateToken()
    {
        $user = factory(User::class)->create();
        $this->assertNotNull($user);

        $user_id = $user->id;
        $accessToken = $user->createToken('login')->accessToken;

        $response = $this->post(
            $this->baseURL.'user/delete',
            ['password' => 'password'],
            $this->getPostRequestHeaders($accessToken)
        );
        $response->assertStatus(200);

        $this->assertCount(1, $user->shortTokens);

        $token = $response->json('data')['token'];

        $shortToken = $user->shortTokens->first();
        $shortToken->expires_at = now();
        $shortToken->save();

        $response = $this->delete(
            $this->baseURL.'user/delete',
            ['token' => $token],
            $this->getPostRequestHeaders($accessToken)
        );

        $response->assertStatus(401);

        $user = User::find($user_id);
        $this->assertNotNull($user);
    }

    protected function getPostRequestHeaders(string $accessToken = ''): array
    {
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Authorization' => 'Bearer '.$accessToken,
        ];
    }
}
